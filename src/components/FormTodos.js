/**
 * Composant qui concerne le formulaire d'ajout d'un Todo
 */

import React from 'react'
import { connect } from 'react-redux'
import { addTodo } from './../actions'

/**
 * Cette constante est connecté aux container pour pouvoir afficher la state
 * 
 * @param {Object} param - Payload de notre constante
 */
const FormTodos = ({ dispatch }) => {
    let title

    return (
        <form
            onSubmit={e => {
                e.preventDefault()
                dispatch(addTodo(title.value))
                title.value = null
            }}    
        >
            <input placeholder="Titre" ref={node => {(title = node)}} />
            <button type="submit">+</button>
        </form>
    )
}

export default connect()(FormTodos)