/**
 * Composant qui retourne la liste de nos Todos
 */

import React from 'react'

/**
 * Cette constante est connecté aux container pour pouvoir afficher la state
 * 
 * @param {Object} param - Payload de notre constante
 */
const ListTodos = ({ todos, onTodoClick }) => (
    <ul>
        {todos.map((todo, i) => {
            return (
            <li 
                key={i} 
                style={{
                    textDecoration: todo.completed ? 'line-through' : 'none'
                }}
            >
                {todo.title} <span className="check-icon" onClick={() => onTodoClick(todo.id)}>{ !todo.completed ? 'Cocher' : 'Decocher'}</span>
            </li>
            )
        })}
    </ul>
)

export default ListTodos