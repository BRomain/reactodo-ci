/**
 * Répertorie toutes les actions présentes dans les reducers
 * Ils indiquent les paramètre à prendre pour notre méthode présent dans le Reducer
 */


import actions from './../constants'

let nextTodoId = 1

/**
 * Ajout la todo à la state de l'application
 * 
 * @param {String} title - Titre de la Todo
 */
export const addTodo = (title) => {
    nextTodoId = nextTodoId + 1
    
    return {
        type: actions.TodosActions.ADD_TODO, // Designe le type qu'on devra faire appel dans nos reducer
        id: nextTodoId,
        title
    }
}

/**
 * Ferme une todo et la met en l'etat de completer
 * 
 * @param {int} id - Id de la todo que l'on veut fermer
 */
export const closeTodo = (id) => ({
    type: actions.TodosActions.CLOSE_TODO,
    id
})