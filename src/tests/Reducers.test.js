import todos from './../reducers/todos'

let id = 1

/**
 * Génére une fake Data de Todo
 * 
 * @param {String} title - Titre de la Todo
 * @param {Boolean} completed - Désigne si la Todo est complétée ou pas
 */
const generateFakeTodo = (title, completed = false) => {
    id = id + 1

    return {
        id,
        title,
        completed
    }
}

describe('Redux - Reducers', () => {
    it('addTodo (state non vide)', () => {
        // Action
        const action = {
            type: 'ADD_TODO',
            title: 'Test',
            id: 3
        }

        const firstValue = generateFakeTodo('Titre')
        const secondValue = generateFakeTodo('Test')

        // Valeur attendue
        const expectedAddTodo = [
            firstValue,
            secondValue
        ]
        const addTodo = todos([firstValue], action)
        expect(addTodo).toEqual(expectedAddTodo)
    });
    it('addTodo (state vide)', () => {
        // Action
        const action = {
            type: 'ADD_TODO',
            title: 'Titre',
            id: 4
        }

        const firstValue = generateFakeTodo('Titre')

        // Valeur attendue
        const expectedAddTodo = [
            firstValue
        ]
        const addTodo = todos([], action)
        expect(addTodo).toEqual(expectedAddTodo)
    });  
})
