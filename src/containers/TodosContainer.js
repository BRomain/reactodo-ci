/**
 * Un container permet de définir les state et méthodes et les liés à un composant
 * 
 * Dans notre cas, on à besoin de récupérer le state.todos et l'injecter dans notre composant ListTodos
 */
import { connect } from 'react-redux'
import ListTodos from '../components/ListTodos'
import { closeTodo } from './../actions'

// Designe la state qui sera 'injecter' dans le composant
const mapStateToProps = state => {
    return {
        todos: state.todos
    }
};

// Méthodes qui seront 'injecter' dans le composant
const mapDispatchToProps = (dispatch) => {
    return {
        onTodoClick: (id) => {
            dispatch(closeTodo(id))
        }
    }
}

// Cette méthode connect, permettra de lier le(s) state/methode(s) au composant passé (ListTodos dans notre exemple) 
export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ListTodos)