/**
 * Un Reducer est un element de notre State global
 * 
 * Dans ce cas cela représente les données qu'on va recevoir tout au courant de la vie de l'application
 * 
 */

import TodosActions from '../constants/TodosActions'

// Représente l'etat de départ des Todos, dans ce cas on en met un ex exemple
const initialState = [
    { id: 1, title: 'Premier todo', completed: false }
]

/**
 * Représente les différentes actions de notre Reducer
 * 
 * @param {?Array} state - State actuel de l'application
 * @param {Objet} action - Représente un objet qui contient le type et les différents argument qu'a besoin notre action
 */
const todos = (state = initialState, action) => {
    switch(action.type) {
        /**
         * Ajoute une Todo dans la state et retourne cette state
         */
        case TodosActions.ADD_TODO:
            return [
                ...state,
                {
                    id: action.id,
                    title: action.title,
                    completed: false
                }
            ]
        case TodosActions.CLOSE_TODO:
            return state.map(todo =>
                todo.id === action.id ? { ...todo, completed: !todo.completed } : todo
            )
        default:
            return state
    }
}

export default todos