import React, { Component } from 'react';
import './App.css';
// Composants
import TodosContainer from './containers/TodosContainer'
import FormTodos from './components/FormTodos'

class App extends Component {
  render() {
    return (
      <div className="todos">
        <FormTodos/>
        <TodosContainer/>
      </div>
    );
  }
}

export default App;
