export default {
    ADD_TODO: 'ADD_TODO',
    DELETE_TODO: 'DELETE_TODO',
    CLOSE_TODO: 'CLOSE_TODO'
}