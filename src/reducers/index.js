import { combineReducers } from 'redux';

// Reducers
import todos from './todos'

// Combien tous les reducers et renvoie un state global (ex: state.todos)
export default combineReducers({
    todos
})