import * as actions from './../actions'

describe('Redux - Actions', () => {
    it('addTodo', () => {
        const expectedAddTodo = {
            type: 'ADD_TODO',
            id: 2,
            title: 'Titre',
        }
        expect(actions.addTodo('Titre')).toEqual(expectedAddTodo)
    });    
    it('closeTodo', () => {
        const expectedAddTodo = {
            type: 'CLOSE_TODO',
            id: 1,
        }
        expect(actions.closeTodo(1)).toEqual(expectedAddTodo)
    });    
})
